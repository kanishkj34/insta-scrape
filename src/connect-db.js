require("./import-db-models");
const debug = require("debug")("db:conn");
const mongoose = require("mongoose");
const config = require('config');

const dbConfig = config.get('db');

let db = mongoose.connect(dbConfig.uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
});

// mongoose.set("debug", true);
mongoose.connection.on("connected", () => {
  debug("Connected to database, insta_scrape");
});
mongoose.connection.on("disconnected", () => {
  debug("Disconnected from database");
});
