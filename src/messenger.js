require("./connect-db");
const Queue = require('bull');
const mongoose = require("mongoose");
const debug = require("debug")("app:messenger");
const debugErr = require("debug")("err:messenger");
const config = require('config');

const redisConfig = config.get('redis');
const messenger = new Queue('messenger', redisConfig.url);

const Post = mongoose.model("Post");

messenger.process(10, async (job) => {
    debug("job", job.data.id);
    let metaData = job.data.metaData;
    let id = job.data.id;
    let post = await find(id);
    if (post) {
        post.metaData = metaData;
        post.save();
    } else {
        post = new Post();
        post.postId = id;
        post.metaData = metaData;
        post.save();
    }
});

async function find(id) {
    let post = await Post.find({ postId: id });
    if (post.length == 0) {
        return false;
    } else {
        return post[0];
    }
}