const Queue = require('bull');
const HttpsProxyAgent = require('https-proxy-agent');
const { ConcurrencyManager } = require("axios-concurrency");
const debug = require("debug")("app:scrape");
const debugErr = require("debug")("err:scrape");
const config = require('config');

const scrapeConfig = config.get('scrape');
const redisConfig = config.get('redis');
const messenger = new Queue('messenger', redisConfig.url);

async function scrapeTag(tag, api) {
    let end_cursor;
    let has_next_page = true;
    while (true) {
        let data = await getData(end_cursor, tag, api);
        if (!data) break;
        end_cursor = data.page_info.end_cursor;
        has_next_page = data.page_info.has_next_page;
        for (let i = 0; i < data.edges.length; i++) {
            let metaData = data.edges[i].node;
            let id = metaData.id;
            messenger.add({
                id,
                metaData
            });
        }
        if (!has_next_page) break;
    }
}


async function getData(end_cursor, tag, api) {
    let tagUrl = `/explore/tags/${tag}/?__a=1`;
    let suffix = end_cursor ? `&max_id=${end_cursor}` : '';
    let reqUrl = tagUrl + suffix;
    let proxy = scrapeConfig.proxyURL;
    var agent = new HttpsProxyAgent(proxy);
    let res;
    try {
        res = await api.get(reqUrl, {
            httpsAgent: agent
        });
    } catch (e) {
        debugErr("error", e);
        throw new Error(e);
    }
    let page_info = res.data.graphql.hashtag.edge_hashtag_to_media.page_info;
    let edges = res.data.graphql.hashtag.edge_hashtag_to_media.edges;
    page_info = page_info ? page_info : { has_next_page: false, end_cursor: null };
    return {
        page_info,
        edges
    }
}


module.exports = { scrapeTag };