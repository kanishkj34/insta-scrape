var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");
var PostSchema = new mongoose.Schema({
    postId: {
        type: String,
        required: [true, "postId is required"],
        index: true,
        unique: true,
    },
    metaData: {
        type: Object,
    },
});

PostSchema.plugin(uniqueValidator, { message: "It is already present in the database." });

mongoose.model("Post", PostSchema);
