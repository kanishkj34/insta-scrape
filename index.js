const axios = require("axios");
const config = require("config");
const csv = require("neat-csv");
const fs = require("fs");
const debug = require("debug")("app:scrape");
const debugErr = require("debug")("err:scrape");
const { scrapeTag } = require("./src/tag");
const { ConcurrencyManager } = require("axios-concurrency");

const scrapeConfig = config.get("scrape");

let tags;
let api = axios.create({
  baseURL: scrapeConfig.baseURL,
});
const manager = ConcurrencyManager(api, scrapeConfig.maxConcurrency);

fs.readFile(scrapeConfig.source, async (err, data) => {
  if (err) {
    debugErr(err);
    return;
  }
  let res = await csv(data);
  tags = res.map((obj) => obj.hashtags);
  debug(`Source File: ${scrapeConfig.source}`);
  debug(`Starting to scrape ${tags.length} hashtags`);
  (async () => {
    tags.forEach(async (tag) => {
      await scrapeTag(tag, api);
    });
    manager.detach();
  })();
});
